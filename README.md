# Data processing

## Estructura del proyecto

 - **App** -> La lógica del proyecto, en este caso consiste en dos Objects con métodos main y corresponde con cada una de las actividades realizadas. Hace uso de los servicios.
 
 - **Services** -> Disponen los métodos necesarios para efectuar la lógica de la aplicación, y permite las operaciones con el resto de elementos del sistema como Spark, o operaciones I/O.
 
 - **Models** -> Case class. Permiten modelar los datos de entrada y salida tanto para el proceso en batch como para el procesado en stream.
 
 - **Views** ->  Permiten la transformación de los datos para los distintos modelos o servicios definidos con anterioridad.


## Ejecutar

El proyecto hace uso de Kafka para realizar el envío de mensajes hacia otros servicios, como puede ser un mailer. Para facilitar la ejecución se ha creado un script, **run.sh**.
Este script hace uso de **services.envar**, en el cuál se definen las variables de configuración. En este caso:

	export KAFKA_HOME=path-de-kafka
	export KAFKA_HOST=localhost
	export KAFKA_PORT=9092

	export APPNAME=flat-increase-measurement
	export TOPIC=increase


Una vez definidas correctamente para el entono, se puede iniciar la aplicación con 
``bash run.sh``

Puesto que no es propósito de este ejercicio el envío de correo, el driver program se encarga de una vez obtenido los datos, enviar mensajes, cuyo payload es un JSON válido y puede ser consumido por otro servicio, por ejemplo un mailer en NodeJS.

### app.Mean
Calcula el precio medio por metro cuadrado y región y almacena el resultado dentro del directorio real-state

### app.Stream
Realiza la agrupación del precio medio por región y calcula la mayor diferencia. Por lo tanto si se añaden ficheros .json al directorio real-state, se produce este cálculo. Permite simular un histórico agregado.