package app

import org.apache.log4j.Level
import org.apache.spark.sql.types.StructType
import utils.Directory._
import services.{JSON, Logs, SparkContext}
import services.IncreaseService._
import cfg.Config._
import views.KafkaView._


object Stream extends SparkContext with Logs {

  def main(args: Array[String]): Unit = {
    val spark = getSparkSession()
    import spark.implicits._

    setLogLevel(Level.ERROR)

    val schema = new StructType()
      .add("location", "string")
      .add("averagePrice", "Float")

    val df = JSON.readStream(path=getDirectory("real-state"), schema=schema)
    val increases = getIncreases(df)

    val monitoring = increases
      .writeStream
      .outputMode("complete")
      .format("console")
      .start

    val notifications = filterIncreases(increases)
      .map(parseToKafka)
      .writeStream
      .outputMode("complete")
      .format("kafka")
      .option("kafka.bootstrap.servers", s"$KAFKA_HOST:$KAFKA_PORT")
      .option("topic", TOPIC)
      .start

    monitoring.awaitTermination()
    notifications.awaitTermination()
    spark.stop()
  }
}
