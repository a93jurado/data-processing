package app

import org.apache.log4j.Level
import org.apache.spark.sql.functions._

import views.FlatView._
import services.{CSV, JSON, Logs, SparkContext}
import utils.Directory._

/**
 * Guarda en un fichero JSON el precio medio de m2 de una localidad
 *
 * Genera un dataset de tipo Flat, sobre el que se efectúa la operación de average
 * Almacena el resultado en un fichero JSON.
 */
object Mean extends SparkContext with Logs {
  def main(args: Array[String]): Unit = {
    val spark = getSparkSession()
    import spark.implicits._

    setLogLevel(Level.ERROR)

    val raw = CSV.read(path=s"${getDirectory(folder= "datasets")}RealEstate.csv")
    val ds = raw.map(r => parseFlat(r))
    val mean = ds
      .groupBy($"location")
      .agg(avg($"priceM2").as("averagePrice"))

    JSON.save(df=mean, path=getDirectory(folder= "real-state"))
    spark.stop()
  }
}
