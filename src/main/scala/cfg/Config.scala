package cfg

import scala.util.Properties.envOrElse

object Config {
  val DOLLAR_TO_EURO_RATIO = 0.89f
  val SQFEET_TO_SQM_RATIO = 0.09290304f

  val APP_MASTER = "local[*]"
  val APP_NAME = envOrElse("APPNAME", "dp")

  val INCREASE_LIMIT= envOrElse("INCREASE_LIMIT", "0.1").toFloat

  val TOPIC = envOrElse("TOPIC", "increase")

  val KAFKA_HOST = envOrElse("KAFKA_HOST", "localhost")
  val KAFKA_PORT = envOrElse("KAFKA_PORT", "9092")
}