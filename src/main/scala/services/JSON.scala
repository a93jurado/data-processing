package services

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StructType

/**
 * Servicio para trabajar con JSON
 *
 */
object JSON extends SparkContext {
  val spark = getSparkSession()

  def save(df: DataFrame, path:String, partitions:Int = 1): Unit = {
    df.coalesce(partitions)
      .write
      .mode("append")
      .json(path)
  }

  def readStream(path: String, schema: StructType): DataFrame = {
    spark
      .readStream
      .format("json")
      .schema(schema)
      .load(path)
  }
}
