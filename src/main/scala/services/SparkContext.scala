package services

import org.apache.spark.SparkConf
import org.apache.spark.sql.{SparkSession}

import cfg.Config._
import utils.Directory._

/**
 * Servicio para manejar el spark context
 *
 */
trait SparkContext {
  private val conf: SparkConf = new SparkConf()
    .setMaster(APP_MASTER)
    .setAppName(APP_NAME)
    .set("spark.sql.streaming.checkpointLocation", getDirectory("checkpoint"))


  def getSparkSession() : SparkSession = {
    SparkSession.builder.config(conf).getOrCreate()
  }
}