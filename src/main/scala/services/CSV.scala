package services

import org.apache.spark.sql.DataFrame

/**
 * Servicio para manejar CSV
 *
 */
object CSV extends SparkContext {
  val spark = getSparkSession()

  def read(path: String, header: Boolean = true): DataFrame = {
    spark
      .read
      .format("com.databricks.spark.csv")
      .option("inferSchema", "true")
      .option("header", header)
      .load(s"file:///${path}")
  }
}
