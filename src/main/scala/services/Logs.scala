package services

import org.apache.log4j._

/**
 * Logger service
 *
 */
trait Logs {
  private[this] val logger = Logger.getRootLogger

  import org.apache.log4j.Level._

  def setLogLevel(level: Level = Level.ERROR) = logger.setLevel(level)

  def debug(message: => String) = if (logger.isEnabledFor(DEBUG)) logger.debug(message)

  def info(message: => String) = if (logger.isEnabledFor(INFO)) logger.info(message)

  def warn(message: => String) = if (logger.isEnabledFor(WARN)) logger.warn(message)

  def error(message: => String) = if (logger.isEnabledFor(ERROR)) logger.error(message)
}