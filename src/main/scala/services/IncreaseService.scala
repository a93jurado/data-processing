package services

import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.functions.collect_list
import views.IncreaseView.parseIncrease
import models.Increase

object IncreaseService extends SparkContext  {
  def getIncreases(df: DataFrame): Dataset[Increase] = {
    val spark = getSparkSession()
    import spark.implicits._

    return df.groupBy("location")
      .agg(collect_list("averagePrice") as "historicAveragePrice")
      .map(r => parseIncrease(r))
      .sort($"value".desc)
  }

  def filterIncreases(ds: Dataset[Increase], limit: Float = 0.1f):Dataset[Increase] = ds.filter(r => r.value > limit)
}
