package views

import org.apache.spark.sql.Row
import cfg.Config.{DOLLAR_TO_EURO_RATIO, SQFEET_TO_SQM_RATIO}
import models.Flat

/**
 * Vista que permite la conversion de unidades, de Row -> Flat
 *
 */
object FlatView {

  private def parsePrice(dollar: Float): Float = dollar * DOLLAR_TO_EURO_RATIO

  private def parseSize(sqFeet: Float): Float = sqFeet * SQFEET_TO_SQM_RATIO

  private def parsePrizeM2(priceSqFeet: Float) : Float = {
    val prizeDollarFeet = parsePrice(priceSqFeet)
    return prizeDollarFeet/SQFEET_TO_SQM_RATIO
  }

  def parseFlat(rawFlat:Row): Flat = {
    val mls:Int = rawFlat.getInt(0)
    val location: String = rawFlat.getString(1).trim
    val price: Float = parsePrice(dollar = rawFlat.getDouble(2).toFloat)
    val bedrooms: Int = rawFlat.getInt(3)
    val bathrooms: Int = rawFlat.getInt(4)
    val size: Float = parseSize(sqFeet = rawFlat.getInt(5).toFloat)
    val prizeM2: Float = parsePrizeM2(priceSqFeet = rawFlat.getDouble(6).toFloat)
    val status: String = rawFlat.getString(7).trim

    return Flat(mls, location, price, bedrooms, bathrooms, size, prizeM2, status)
  }
}