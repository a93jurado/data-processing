package views

import models.{Increase, Message}

import scala.util.parsing.json.JSONObject

/**
 * Vista que permite la serialización de payload a string,
 * JSON a través de mensaje de Kafka
 *
 */
object KafkaView {
  def parseToKafka(increase: Increase): Message = {
    val payload = Map(
      "location" -> increase.location,
      "variation" -> increase.variation,
      "value" -> increase.value,
    )

    return Message(value = JSONObject(payload).toString)
  }
}
