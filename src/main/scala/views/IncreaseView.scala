package views

import models.Increase
import org.apache.spark.sql.Row

/**
 * Vista que permite la conversión Row -> Increase
 *
 * Realiza el caulcula del porcentaje de crecimiento
 */
object IncreaseView {
  private def getRise(list: Seq[Float]): Float = {
    val last = list.size - 1

    if (list.size > 1) {
      val prev = list(last -1)
      val current = list(last)

      return (current - prev) / prev
    }

    return 0f
  }

  def parseIncrease(rawIncrease:Row): Increase = {
    val location = rawIncrease.getString(0)
    val historic = rawIncrease.getSeq[Float](1)
    val variation = getRise(historic)
    val currentValue = historic.last

    return Increase(location, historic, variation, currentValue )
  }
}
