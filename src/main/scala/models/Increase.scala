package models

case class Increase (
                      location: String,
                      historic: Seq[Float],
                      variation: Float,
                      value: Float
                    )