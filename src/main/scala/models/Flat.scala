package models

case class Flat (
                  mls: Int,
                  location: String,
                  price: Float,
                  bedrooms: Int,
                  bathrooms: Int,
                  size: Float,
                  priceM2: Float,
                  status: String,
                )
