package utils

import java.io.File

/**
 * Utilidad para trabajar con paths
 *
 */
object Directory {
  private val currentDirectory = System.getProperty("user.dir")
  private val fs = File.separator

  def getDirectory(folder: String): String = s"${currentDirectory}${fs}${folder}${fs}"

}