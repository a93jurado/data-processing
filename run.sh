#!/bin/bash

set -a
source services.envar

brokers=$(echo dump | nc localhost 2181 | grep brokers)

if [[ -z  ${brokers} ]];
then
    ${KAFKA_HOME}/bin/zookeeper-server-start.sh ${KAFKA_HOME}/config/zookeeper.properties > zookerper.log &

    ${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_HOME}/config/server.properties > kafka.log &
fi

topics=$(${KAFKA_HOME}/bin/kafka-topics.sh --list --zookeeper localhost:2181 | grep ${TOPIC})

if [[ -z ${topics} ]];
then
${KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic ${TOPIC}
fi


sbt compile

sbt "runMain app.Mean"
sbt "runMain app.Stream"
